from django.contrib import admin
from .models import Note

# register Note model
admin.site.register(Note)