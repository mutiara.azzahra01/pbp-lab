import 'package:flutter/material.dart';

import '../screens/filters_screen.dart';
import '../screens/aboutus_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
        color: Colors.black,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CircleAvatar(
                  backgroundImage: NetworkImage('https://i.pinimg.com/originals/94/3a/71/943a71ad89c3ec4906a7be3e2050e7ad.jpg'),
                  radius: 40.0,
                  // backgroundColor: Colors.white,
                ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Tyas',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 25.0
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Text('tyas@gmail.com',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 14.0
                        ),
                      ),
                    ],
                  )
                ],
              ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Masakan', Icons.restaurant, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Filter', Icons.settings, () {
            Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('About Us', Icons.info, () {
            Navigator.of(context).pushReplacementNamed(AboutUsScreen.routeName);
          }),
        ],
      ),
    );
  }
}
