import 'package:flutter/material.dart';
import '../models/testimoni.dart';

class TestimoniScreen extends StatelessWidget {
  final data = [
    Testimoni(
      id: "1",
      testimoni: "Saaangaaaatttt bagusssss!!!!!",
      nama: "Tyas",
      kelas: "12 IPA"
    ),
    Testimoni(
      id: "2",
      testimoni: "MANTULLL!!!!",
      nama: "Anonim",
      kelas: "11 IPS"
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: data.length, 
        //LOOPING DATA
        itemBuilder: (context, i) {
          return Card(
            elevation: 10,
            child: ListTile(
              title: Text(
                data[i].testimoni,
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
              ),
              subtitle:
                  Text('${data[i].nama} kelas ${data[i].kelas}',
                  style: TextStyle(color: Colors.white)),
            ),
          );
        },
      ),
    );
  }
}