class Testimoni {
  String id;
  String testimoni;
  String nama;
  String kelas;

  Testimoni({
    this.id,
    this.testimoni,
    this.nama,
    this.kelas
  });
  
  factory Testimoni.fromJson(Map<String, dynamic> json) => Testimoni(
    id: json['id'],
    testimoni: json['testimoni'],
    nama: json['nama'],
    kelas: json['kelas'],
  );
}