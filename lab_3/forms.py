from django import forms
from lab_1.models import Friend

# buat class FriendForm
class FriendForm (forms.ModelForm):
    class Meta:
        model = Friend
        fields =  ['name', 'npm', 'DOB']

        widgets = {
            'DOB': forms.DateInput(attrs={'type': 'date'})
        }
