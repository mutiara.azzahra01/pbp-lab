from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/admin/login/') # arahkan ke halaman login apabila belum login
def index (request):
    friends = Friend.objects.all() # ambil query set semua objek friend di database
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
    
# buat method add_friend untuk menambahkan teman
@login_required(login_url='/admin/login/')
def add_friend(request):
    if request.method == "POST":
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')

    form = FriendForm
    return render(request, 'lab3_form.html', {'form':form})