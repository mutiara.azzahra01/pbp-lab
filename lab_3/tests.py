from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_friend


class Lab2UnitTest(TestCase):

    # tes apakah halaman web index mengarah ke halaman login
    def test_index_is_exist(self):
        response = Client().get('/lab-3/')
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/lab-3/')
        self.assertEqual(found.func, index)

    # tes apakah halaman web add_friend mengarah ke halaman login
    def test_add_friend_is_exist(self):
        response = Client().get('/lab-3/add')
        self.assertEqual(response.status_code, 302)

    def test_using_add_friend_func(self):
        found = resolve('/lab-3/add')
        self.assertEqual(found.func, add_friend)
        

