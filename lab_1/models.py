from django.db import models

# buat friend model dengan atribut nama, npm, dan DOB
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=30)
    DOB = models.DateField()
