from django.urls import path
from .views import index
from lab_1 import views

urlpatterns = [
    path('', index, name='index'),
    # tambahkan path friends menggunakan friend_list dari views
    path('lab-1/friends', views.friend_list, name='friend_list')
]
