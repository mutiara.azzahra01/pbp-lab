from django.contrib import admin
from .models import Friend

# register friend model
admin.site.register(Friend)
