from django import forms
from django.forms import fields
from lab_2.models import Note


# buat class NoteForm
class NoteForm (forms.ModelForm):
    
    to = forms.CharField(label='to', widget=forms.TextInput(attrs={'placeholder': 'Maximum 50 characters', 'class': 'form_input'}))
    fromm = forms.CharField(label='from', widget=forms.TextInput(attrs={'placeholder': 'Maximum 50 characters', 'class': 'form_input'}))
    title = forms.CharField(label='title', widget=forms.TextInput(attrs={'placeholder': 'Maximum 50 characters', 'class': 'form_input'}))
    message = forms.CharField(label='message', widget=forms.Textarea(attrs={'placeholder': 'Enter a message', 'class': 'form_input'}))
    
    class Meta:
        model = Note
        fields = '__all__'

