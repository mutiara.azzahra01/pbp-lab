import 'package:flutter/material.dart';
import 'package:lab_7/screens/testimoni_form.dart';
import './screens/testimoni.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "BIMBOL",
        home: TestimoniScreen(),
    );
  }
}