import 'package:flutter/material.dart';


class TestimoniFormScreen extends StatefulWidget {

  TestimoniFormScreen();

  @override
  _TestimoniFormScreenState createState() => _TestimoniFormScreenState();
}

class _TestimoniFormScreenState extends State<TestimoniFormScreen> {
    String? _nama = "";
    String? _kelas = "";
    String? _testimoni = "";

    final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("BIMBOL • Bimbel Online di Masa Pandemi"),
        backgroundColor: Colors.pink[200],
      ),
      // form testimoni
       body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: Susilo Bambang",
                      labelText: "Nama Lengkap",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                    // menyimpan input ke variabel _nama
                    onChanged: (value) => setState(() => _nama = value),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: 12 IPA, 11 IPS",
                      labelText: "Kelas",
                      icon: Icon(Icons.school),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Kelas tidak boleh kosong';
                      }
                      return null;
                    },
                    // menyimpan input ke variabel _kelas
                    onChanged: (value) => setState(() => _kelas = value),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    minLines: 6, 
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    decoration: new InputDecoration(
                      labelText: "Testimoni",
                      icon: Icon(Icons.reviews),
                      
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        
                        return 'Testimoni tidak boleh kosong';
                      }
                      return null;
                      
                      
                    },
                    // menyimpan input ke variabel _testimoni
                    onChanged: (value) => setState(() => _testimoni = value),
                  ),
                ),

                RaisedButton(
                  child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  color: Colors.pink[200],
                  onPressed: () { 
                    // jika form sudah terisi semua, maka akan menampilkan dialog box 'SUCCESS!'
                    if (_formKey.currentState!.validate()) {
                      showDialog(
                      context: context,
                      builder: (context){
                      return Dialog(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 16),
                              Text(
                                'SUCCESS!',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                              SizedBox(height: 16),
                              ElevatedButton(
                                child: Text('Ok'),
                                onPressed: () => Navigator.of(context).pop(),
                              )
                            ],
                          ),
                        ),
                      );
                      }
                    );
                    // dan akan mencetak input dari tiap fieldnya
                    print(_nama);
                    print(_kelas);
                    print(_testimoni);
                    }
                    
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}