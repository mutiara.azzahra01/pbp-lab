import 'package:flutter/material.dart';
import '../models/testimoni_model.dart';
import 'testimoni_form.dart';

class TestimoniScreen extends StatelessWidget {
  // data untuk ditampilkan pada laman testimoni
  final data = [
    Testimoni(
      id: "1",
      testimoni: "Saaangaaaatttt bagusssss!!!!!",
      nama: "Tyas",
      kelas: "12 IPA"
    ),
    Testimoni(
      id: "2",
      testimoni: "MANTULLL!!!!",
      nama: "Anonim",
      kelas: "11 IPS"
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BIMBOL • Bimbel Online di Masa Pandemi'),
        backgroundColor: Colors.pink[200],
      ),
      
      body: ListView.builder(
        itemCount: data.length, 
        //LOOPING DATA
        itemBuilder: (context, i) {
          
          return Card(
            elevation: 10,
            child: ListTile(
              title: Text(
                data[i].testimoni,
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
              ),
              subtitle:
                  Text('${data[i].nama} kelas ${data[i].kelas}',
                  style: TextStyle(color: Colors.black)),
            ),
          );
        },
      ),
      // button untuk ke laman form tambah testimoni
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TestimoniFormScreen()));
        },
        label: const Icon(Icons.add),
        backgroundColor: Colors.pink[200],
      ),
    );
  }
}

