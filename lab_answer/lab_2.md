1. Apakah perbedaan antara JSON dan XML?
   1. JSON tidak memiliki tag awal dan akhir serta sintaksnya lebih ringan daripada XML.
   2. JSON mendukung tipe data teks dan angka seperti bilangan bulat dan string. Sedangkan XML mendukung banyak tipe data seperti angka, teks, gambar, grafik, dan sebagainya.
   3. Data pada JSON terstruktur karena menggunakan array. Sedangkan XML tidak memiliki dukungan langsung untuk tipe array.
   4. JSON berorientasi data, sedangkan XML berorientasi dokumen.
   5.  Sesuai kepanjangannya, yaitu "Extensive Markup Language", XML merupakan bahasa markup yang digunakan untuk menambahkan info tambahan ke teks biasa, sedangkan JSON adalah cara yang efisien untuk merepresentasikan data terstruktur dalam format yang dapat dibaca manusia.
   6. Pengembang JSON ditentukan oleh Douglas Crockford sedangkan pengembang XML adalah Konsorsium World Wide Web.

2. Apakah perbedaan antara HTML dan XML?
    1. XML berfokus pada transfer data atau informasi sedangkan HTML difokuskan pada penyajian data.
    2. XML strict untuk tag penutup sedangkan HTML tidak strict.
    3. XML itu Case Sensitive sedangkan HTML Case Insensitive.
    4. Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
    5. XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
    6. XML tidak boleh ada error, sedangkan HTML error kecil dapat diabaikan.